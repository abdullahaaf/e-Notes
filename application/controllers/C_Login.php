<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class C_Login extends CI_Controller
{

  function __construct()
  {
    parent::__construct();

    if ($this->session->userdata('level')=='admin')
    {
      redirect(site_url('admin/C_Dashboard'));
    }
    if ($this->session->userdata('level')=='operator')
    {
      redirect(site_url('operator/C_Dashboard_Operator'));
    }
  }

  public function index($error = NULL)
  {
    $data = array(
      'action'=>site_url('C_Login/login'),
      'error'=>$error
    );
    $this->load->view('V_Login',$data);
  }

  public function login()
  {
    $this->load->model('M_Login','model');
        $login = $this->model->login($this->input->post('email'), $this->input->post('password'));

    if ($login == 1)
    {
      $row = $this->model->data_login($this->input->post('email'),
      $this->input->post('password'));

      // create session
      $data = array(
        'e-Notes'=>TRUE,
        'nama'=>$row->nama,
        'email'=>$row->email
      );
      $this->session->set_userdata($data);
      if ($row->level=="admin"){
        redirect(site_url('admin/C_Dashboard'));
        // echo "sukses login";
      }
      if ($row->level=="operator"){
        redirect(site_url('operator/C_Dashboard_Operator'));
      }
    }else
    {
      $error = '<script> alert("Email atau password anda salah")</script>';
      // echo "login gagal";
        $this->index($error);
    }
  }

  function logout()
  {
    $this->session->sess_destroy();
    redirect(site_url('C_Login'));
  }
}
