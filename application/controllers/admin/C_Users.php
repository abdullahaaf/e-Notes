<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class C_Users extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('M_Users');

    if ($this->session->userdata('e-Notes')<>1) {
            redirect(site_url('C_Login'));
        }
  }

  function index()
  {
    $data['user'] = $this->M_Users->get_all_users();
    $this->load->view('Header_Admin/Header');
    $this->load->view('Sidebar_Admin/Sidebar');
    $this->load->view('menu-admin/V_Users', $data);
  }

  public function add_user()
  {
    $data = array(
      'nama' => $this->input->post('nama'),
      'alamat' => $this->input->post('alamat'),
      'email' => $this->input->post('email'),
      'no_telp' => $this->input->post('no_telp'),
      'password' => $this->input->post('password'),
      'level' => $this->input->post('level'),
    );

    $insert = $this->M_Users->add_user($data);
    echo json_encode(array("status"=>TRUE));
  }

  public function edit_user($id_user)
  {
    $data = $this->M_Users->get_by_id($id_user);
    echo json_encode($data);
  }

  public function update_user()
  {
    $data = array(
      'nama' => $this->input->post('nama'),
      'alamat' => $this->input->post('alamat'),
      'email' => $this->input->post('email'),
      'no_telp' => $this->input->post('no_telp'),
      'password' => $this->input->post('password'),
      'level' => $this->input->post('level'),
    );

    $this->M_Users->update_user(array('id_user'=>$this->input->post('id_user')), $data);
    echo json_encode(array("status"=>TRUE));
  }

  public function delete_user($id_user)
  {
    $this->M_Users->delete_by_id($id_user);
    echo json_encode(array('status'=>TRUE));
  }
}
