<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class C_Dashboard extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_Jadwal_Rapat');
    $this->load->model('M_Login',TRUE);
    $this->load->helper('url');

    if ($this->session->userdata('e-Notes')<>1) {
            redirect(site_url('C_Login'));
        }
  }

  public function index()
  {
    $data['jadwal'] = $this->M_Jadwal_Rapat->get_all_jadwal();
    $this->load->view('Header_Admin/Header');
    $this->load->view('Sidebar_Admin/Sidebar');
    $this->load->view('menu-admin/V_Dashboard', $data);
  }
}
