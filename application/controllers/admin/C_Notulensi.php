<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class C_Notulensi extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_Notulensi','notulensi');

    if ($this->session->userdata('e-Notes')<>1) {
            redirect(site_url('C_Login'));
        }
  }

  function index()
  {
    $data['notulen'] = $this->notulensi->semuaNotulensi();
    $this->load->helper('url');
    $this->load->view('Header_Admin/Header');
    $this->load->view('Sidebar_Admin/Sidebar');
    $this->load->view('menu-admin/V_Notulensi', $data);
  }

  public function tambahNotulensi()
  {
    $data = array(
      "judul"=>$this->input->post('txtnamaRapat'),
      "tanggal_notulensi"=>$this->input->post('txtTanggal'),
      "deskripsi"=>$this->input->post('txtDeskripsi'),
      "permission"=>$this->input->post('hakAkses'),
      "user_id"=>"1");
    $tambah = $this->notulensi->tambahNotulensi($data);
    if ($tambah>=1) {
      $this->session->set_flashdata('sukses','Data Notulensi berhasil ditambahkan');
      redirect('admin/C_Notulensi');
    }else{
      $this->session->set_flashdata('gagal','terjadi kesalah pada proses notulensi');
      redirect('admin/C_Notulensi');
    }
  }

  public function editNotulen($id)
  {
    $data = $this->notulensi->pilihNotulensi($id);
    $dataEdit = array(
      "id_notulensi"=>$data[0]['id_notulensi'],
      "judul"=>$data[0]['judul'],
      "tanggal"=>$data[0]['tanggal_notulensi'],
      "deskripsi"=>$data[0]['deskripsi'],
      "user_id"=>$data[0]['operator_id']);
    $this->load->view('Header_Admin/Header');
    $this->load->view('Sidebar_Admin/Sidebar');
    $this->load->view('E_Notulensi', $dataEdit);
  }

  public function UpdateNotulensi()
  {
    $id = $this->input->post('id_notulen');
    $dataUpdate = array(
      "judul"=>$this->input->post('editnamaRapat'),
      "tanggal_notulensi"=>$this->input->post('editTanggal'),
      "deskripsi"=>$this->input->post('editDeskripsi'),
      "permission"=>$this->input->post('edithakAkses'),
      "user_id"=>"1");
    $update = $this->notulensi->UpdateNotulensi($dataUpdate, $id);
    if ($update) {
      $this->session->set_flashdata('sukses','Data notulensi berhasil di perbarui');
      redirect('admin/C_Notulensi');
    }else{
      $this->session->set_flashdata('gagal','Terjadi kesalahan pada proses hapus data');
      redirect('admin/C_Notulensi');
    }
  }

    public function hapusNotulensi($id)
    {
      $data = $this->notulensi->hapusNotulensi($id);
      if ($data) {
        $this->session->set_flashdata('hapus','Data Notulensi Berhasil Dihapus.');
        redirect('admin/C_Notulensi');
      }else{
        $this->session->set_flashdata('gagal','Data Notulensi Gagal Dihapus.');
        redirect('admin/C_Notulensi');
      }
    }

}
?>
