<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Home extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('M_Jadwal_Rapat');
  }

  public function index()
  {
    $data['jadwal'] = $this->M_Jadwal_Rapat->get_all_jadwal();
    $this->load->view('V_Home', $data);
  }
}
