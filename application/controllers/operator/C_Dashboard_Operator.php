<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class C_Dashboard_Operator extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_Jadwal_Rapat');
    if ($this->session->userdata('e-Notes')<>1) {
            redirect(site_url('C_Login'));
        }
  }

  public function index()
  {
    $data['jadwal'] = $this->M_Jadwal_Rapat->get_all_jadwal();
    $this->load->helper('url');
    $this->load->view('Header_Operator/Header_Operator');
    $this->load->view('Sidebar_Operator/Sidebar_Operator');
    $this->load->view('menu-operator/V_Dashboard', $data);
  }
}
