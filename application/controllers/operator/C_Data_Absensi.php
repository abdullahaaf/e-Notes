<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class C_Data_Absensi extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_Data_Absensi');

    if ($this->session->userdata('e-Notes')<>1) {
            redirect(site_url('C_Login'));
        }
  }

  function index()
  {
    $data['absensi'] = $this->M_Data_Absensi->get_data_absensi("order by id_abs desc")->result_array();
    $this->load->helper('url');
    $this->load->view('Header_Operator/Header_Operator');
    $this->load->view('Sidebar_Operator/Sidebar_Operator');
    $this->load->view('menu-operator/V_Data_Absensi', $data);
  }

  // function index()
  // {
  //   $data['absensi'] = $this->M_Data_Absensi->get_data_absensi()->result_array();
  //   $this->load->helper('url');
  //   $this->load->view('Header_Admin/Header');
  //   $this->load->view('Sidebar_Admin/Sidebar');
  //   $this->load->view('menu-admin/V_Data_Absensi', $data);
  // }

  public function cekMasuk()
  {
    $this->load->model('M_Data_Absensi');
    $this->M_Data_Absensi->cekMasuk();
  }
}
