<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class C_Data_Peserta extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('M_Data_Peserta');
    $this->load->helper('url');

    if ($this->session->userdata('e-Notes')<>1) {
            redirect(site_url('C_Login'));
        }
  }

  public function index()
  {
    $data['peserta'] = $this->M_Data_Peserta->get_all_peserta();
    $this->load->view('Header_Operator/Header_Operator');
    $this->load->view('Sidebar_Operator/Sidebar_Operator');
    $this->load->view('menu-operator/V_Data_Peserta', $data);
  }

  public function add_peserta()
  {

    $data = array(
      'nama' => $this->input->post('nama'),
      'alamat' => $this->input->post('alamat'),
      'email' => $this->input->post('email'),
      'no_telp' => $this->input->post('no_telp'),
    );

    $insert = $this->M_Data_Peserta->add_peserta($data);
    echo json_encode(array('status'=>TRUE));
  }

  public function edit_peserta($id_prt)
  {
    $data = $this->M_Data_Peserta->get_by_id($id_prt);
    echo json_encode($data);
  }

  public function update_peserta()
  {
    $data = array(
      'nama' => $this->input->post('nama'),
      'alamat' => $this->input->post('alamat'),
      'email' => $this->input->post('email'),
      'no_telp' => $this->input->post('no_telp'),
    );

    $this->M_Data_Peserta->update_peserta(array('id_prt'=>$this->input->post('id_prt')),$data);
    echo json_encode(array('status'=>TRUE));
  }

  public function delete_peserta($id_prt)
  {
    $this->M_Data_Peserta->delete_peserta($id_prt);
    echo json_encode(array('status'=>TRUE));
  }
}
