<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class C_Jadwal_Rapat extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_Jadwal_Rapat');
    $this->load->helper('url');

    if ($this->session->userdata('e-Notes')<>1) {
            redirect(site_url('C_Login'));
        }
  }

  public function index()
  {
    $data['jadwal'] = $this->M_Jadwal_Rapat->get_all_jadwal();
    $this->load->view('Header_Operator/Header_Operator');
    $this->load->view('Sidebar_Operator/Sidebar_Operator');
    $this->load->view('menu-operator/V_Jadwal_Rapat', $data);
  }

  function add_jadwal()
  {
    $data = array(
      'nama_rapat' => $this->input->post('nama_rapat'),
      'tanggal_rapat' => $this->input->post('tanggal_rapat'),
      'jam' => $this->input->post('jam'),
      'keterangan' => $this->input->post('keterangan'),
    );

    $insert = $this->M_Jadwal_Rapat->add_jadwal($data);
    echo json_encode(array('status'=>TRUE));
  }

  public function edit_jadwal($id_jadwal)
  {
    $data = $this->M_Jadwal_Rapat->get_by_id($id_jadwal);
    echo json_encode($data);
  }

  public function update_jadwal()
  {
    $data = array(
      'nama_rapat' => $this->input->post('nama_rapat'),
      'tanggal_rapat' => $this->input->post('tanggal_rapat'),
      'jam' => $this->input->post('jam'),
      'keterangan' => $this->input->post('keterangan'),
    );

    $this->M_Jadwal_Rapat->update_jadwal(array('id_jadwal'=>$this->input->post('id_jadwal')),$data);
    echo json_encode(array('status'=>TRUE));
  }

  public function delete_jadwal($id_jadwal)
  {
    $this->M_Jadwal_Rapat->delete_by_id($id_jadwal);
    echo json_encode(array('status'=>TRUE));
  }
}
