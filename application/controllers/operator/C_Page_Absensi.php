<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class C_Page_Absensi extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  function index()
  {
    $this->load->helper('url');
    // $this->load->view('Header_Admin/Header');
    // $this->load->view('Sidebar_Admin/Sidebar');
    $this->load->view('V_Page_Absensi');
  }
}
