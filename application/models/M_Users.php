<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Users extends CI_Model
{
  var $table = 'users';

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function get_all_users()
  {
    $this->db->from('users');
    $query=$this->db->get();
    return $query->result();
  }

  public function get_by_id($id_user)
  {
    $this->db->from($this->table);
    $this->db->where('id_user',$id_user);
    $query=$this->db->get();

    return $query->row();
  }

  public function add_user($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update_user($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function delete_by_id($id_user)
  {
    $this->db->where('id_user',$id_user);
    $this->db->delete($this->table);
  }
}
