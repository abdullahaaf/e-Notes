<?php
  /**
   *
   */
  class M_Data_Absensi extends CI_Model
  {

    var $table = 'tb_absensi';

    function __construct()
    {
      parent::__construct();
      $this->load->database();
    }

    // function get_data_absensi()
    // {
    //   $this->db->from('tb_absensi');
    //   $query = $this->db->get();
    //   return $query->result();
    // }

    function get_data_absensi($where="")
    {
      $data = $this->db->query('SELECT p.*, q.*
                                  FROM peserta p
                                  INNER JOIN tb_absensi q
                                  ON(p.email = q.email)'.$where);
      return $data;
    }

    function cekEmail()
    {
      $email = $this->input->post('email');
      $query = $this->db->query("select email from peserta where email ='$email'");
      return $query->num_rows();
    }

    function cekMasuk()
    {
      $email = $this->input->post('email');
      $datenow = date("Y-m-d");
      $jammasuk = "";
      $cekemail = $this->cekEmail();
      if($cekemail == 0)
      {
        echo '<hr><p class="text-danger">Email tidak tersedia</p>';
        return false;
      }
      $query = $this->db->query("SELECT email,jammasuk FROM tb_absensi WHERE email ='$email' and tanggal='$datenow' and kodeabsensi='1'");
      if ($query->num_rows() > 0)
      {
        foreach ($query->result() as $data)
        {
          $jammasuk = $data->jammasuk;
        }
        echo'<hr><script type="text/javascript"> alert("Gagal, absensi telah dilakukan !")</script>';
        // echo'<p class="text-danger text-center"><br>'.$jammasuk.' !!! </p><a href="#" class="more"></a>';
				return false;
      }else
      {
          $data = array
          (
            'email'=>$email,
            'kodeabsensi'=>'1',
            'jammasuk'=>date("H:i:s"),
            'tanggal'=>$datenow
          );
          $this->db->trans_start();
          $this->db->insert('tb_absensi', $data);
          $this->db->trans_complete();
          echo'<hr><script type="text/javascript"> alert("Absensi sukses dilakukan !")</script>';
  				// echo'<p class="text-success text-center"><br>'.date("H:i:s").'</p>';
      }
    }
  }
