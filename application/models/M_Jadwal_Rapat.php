<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Jadwal_Rapat extends CI_Model
{

  var $table = 'jadwal_rapat';

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function get_all_jadwal()
  {
    $this->db->from('jadwal_rapat');
    $query = $this->db->get();
    return $query->result();
  }

  public function get_by_id($id_jadwal)
  {
    $this->db->from($this->table);
    $this->db->where('id_jadwal',$id_jadwal);
    $query = $this->db->get();
    return $query->row();
  }

  public function add_jadwal($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update_jadwal($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function delete_by_id($id_jadwal)
  {
    $this->db->where('id_jadwal', $id_jadwal);
    $this->db->delete($this->table);
  }
}
