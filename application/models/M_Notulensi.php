<?php defined('BASEPATH')OR exit('no direct script access allowed');
/**
 *
 */
 class M_Notulensi extends CI_Model
 {

 	function __construct()
 	{
 		parent::__construct();
 	}

 	public function semuaNotulensi()
 	{
 		$data = $this->db->query("SELECT * FROM notulensi ORDER BY id_notulensi DESC");
 		if ($data) {
 			return $data->result_array();
 		}else{
 			return false;
 		}
 		unset($data);
 	}

 	public function pilihNotulensi($id)
 	{
 		$data = $this->db->query("SELECT * FROM notulensi where id_notulensi",$id);
 		if ($data) {
 			return $data->result_array();
 		}else{
 			return false;
 		}
 		unset($data);
 	}

 	public function tambahNotulensi($data)
 	{
 		$result = $this->db->insert('notulensi', $data);
 		if ($result) {
 			return $result;
 		}else{
 			return false;
 		}
 		unset($result);
 	}

 	public function updateNotulensi($data, $id)
 	{
 		$this->db->where('id_notulensi', $id);
 		$this->db->update('notulensi', $data);
 		if ($this->db->affected_rows()>0) {
 			return true;
 		}else{
 			return false;
 		}
 	}

 	public function hapusNotulensi($id)
 	{
 		$this->db->where('id_notulensi', $id);
 		$this->db->delete('notulensi');
 		if ($this->db->affected_rows()>0) {
 			return true;
 		}else{
 			return false;
 		}
 	}
 }

 ?>
