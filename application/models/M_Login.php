<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 */
class M_Login extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function login ($email,$password)
  {
    $this->db->where('email',$email);
    $this->db->where('password',$password);
    // $this->db->where('level',$level);
    $query = $this->db->get('users');
    return $query->num_rows();
  }

  function data_login($email,$password)
  {
    $this->db->where('email',$email);
    $this->db->where('password',$password);
    // $this->db->where('level',$level);
    return $this->db->get('users')->row();
  }
}
