<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  	<!-- VENDOR CSS -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
  	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css')?>">
  	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/linearicons/style.css')?>">
  	<!-- MAIN CSS -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css')?>">
  	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/css/demo.css')?>">
  	<!-- GOOGLE FONTS -->
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
  	<!-- ICONS -->
  	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/apple-icon.png')?>">
  	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assets/img/note-icon.png">
    <!-- DATATABLE -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/datatables/jquery.dataTables.min.css">
    <!-- DATEPICKER -->
    <!-- <link rel="stylesheet" href="<?php echo base_url('assets/datepicker/css/datepicker.css')?>"> -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- TIMEPICKER -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/timepicki/css/timepicki.css">
  <body>
    <div id="wrapper">
      <!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
  <div class="sidebar-scroll">
    <nav>
      <ul class="nav">
        <li><a href="<?php echo site_url('operator/C_Dashboard_Operator')?>"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
        <!-- <li><a href="<?php echo site_url('admin/C_Users')?>"><i class="lnr lnr-user"></i> <span>User</span></a></li> -->
        <li>
          <a href="#subPeserta" data-toggle="collapse" class="collapsed"><i class="fa fa-users"></i> <span>Peserta Rapat</span><i class="icon-submenu lnr lnr-chevron-down"></i></a>
          <div class="collapse" id="subPeserta">
            <ul class="nav">
              <li><a href="<?php echo site_url('operator/C_Data_Peserta')?>" class="fa fa-vcard-o"> Data Peserta</a></li>
              <li><a href="<?php echo site_url('operator/C_Data_Absensi')?>" class="fa fa-pencil-square-o"> Data Absensi Peserta</a></li>
            </ul>
          </div>
        </li>
        <li>
          <a href="#subRapat" data-toggle="collapse" class="collapsed"><i class="fa fa-link"></i> <span>Rapat</span><i class="icon-submenu lnr lnr-chevron-down"></i></a>
          <div class="collapse" id="subRapat">
            <ul class="nav">
              <li><a href="<?php echo site_url('operator/C_Jadwal_Rapat')?>" class="fa fa-calendar-check-o"> Jadwal Rapat</a></li>
              <li><a href="<?php echo site_url('operator/C_Broadcast_Jadwal')?>" class="fa fa-share"> Broadcast Jadwal</a></li>
              <li><a href="<?php echo site_url('operator/C_Notulensi')?>" class="fa fa-pencil"> Notulensi</a></li>
              <!-- <li><a href="<?php echo site_url('operator/C_Page_Absensi');?>" target = "_blank" class="fa fa-check-square-o"> Panel Absensi Rapat</a></li> -->
            </ul>
          </div>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- END LEFT SIDEBAR -->
    </div>

    <!-- javascript -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/timepicki/js/timepicki.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/chartist/js/chartist.min.js"></script>
    <script src="<?php echo base_url();?>assets/scripts/klorofil-common.js"></script>
    <script src="<?php echo base_url();?>assets/datatables/jquery.dataTables.min.js"></script>

  </body>
</html>
