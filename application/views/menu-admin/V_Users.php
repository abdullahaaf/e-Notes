<!DOCTYPE html>
<html>
  <head>
    <title>Users | e-Notes</title>
  </head>
  <body>
    <div id="wrapper">
      <!-- MAIN -->
      <div class="main">
        <!-- main content -->
        <div class="main-content">
          <div class="container-fluid">
            <div class="panel panel-headline">
              <div class="panel-heading">
                <h3 class="panel-title">Data User</h3>
                <p class="panel-subtitle">Admin dapat menambah dan menghapus user yang bisa mengakses sistem</p>
              </div>
              <form class="navbar-form navbar-left">
                <div class="input-group">
                  <span class="input-group-btn"><button type="button" name="button" onclick = "add_user()" class="btn btn-success"><i class="fa fa-user-plus"> Tambah Data User</i></button></span>
                </div>
              </form>
              <div class="panel-body">
                <!-- end header panel -->
                <!-- table hover row -->
                <div>
                  <table class="table table-striped-row" id="table">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Email</th>
                        <th>No Telepon</th>
                        <th>Password</th>
                        <th>Level</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $nomor = 1; ?>
                      <?php foreach($user as $users){?>
  				            <tr>
        				         <td><?php echo $nomor++;?></td>
        				         <td><?php echo $users->nama;?></td>
        								 <td><?php echo $users->alamat;?></td>
          							 <td><?php echo $users->email;?></td>
          							 <td><?php echo $users->no_telp;?></td>
                         <td><?php echo $users->password;?></td>
                         <td><?php echo $users->level;?></td>
          							 <td>
          								 <button class="btn btn-warning btn-sm" onclick="edit_user(<?php echo $users->id_user;?>)"><i class="fa fa-pencil"></i></button>
          								 <button class="btn btn-danger btn-sm" onclick="delete_user(<?php echo $users->id_user;?>)"><i class="fa fa-trash"></i></button>
                         </td>
  				            </tr>
  				            <?php }?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<!-- operasi yang terhubung ke database -->
<script type="text/javascript">

$(document).ready(function (){
  $('#table').DataTable();
});

var save_method;
var table;

  function add_user()
  {
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modal
    $('#modal-form').modal('show'); // show bootstrap modal
  }

  function edit_user(id_user)
  {
    save_method = 'update';
    $('#form')[0].reset();

    // ajax load data from ajax
    $.ajax({
      url : "<?php echo site_url('admin/C_Users/edit_user/')?>/" + id_user,
      type : "GET",
      dataType : "JSON",
      success : function(data)
      {
        $('[name="id_user"]').val(data.id_user);
        $('[name="nama"]').val(data.nama);
        $('[name="alamat"]').val(data.alamat);
        $('[name="email"]').val(data.email);
        $('[name="no_telp"]').val(data.no_telp);
        $('[name="password"]').val(data.password);
        $('[name="level"]').val(data.level);

        $('#modal-form').modal('show');
        $('.page-title').text('Edit Data User');
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function save()
  {
    var url;
    if(save_method =='add')
    {
      url ="<?php echo site_url('admin/C_Users/add_user')?>";
    }else
    {
      url ="<?php echo site_url('admin/C_Users/update_user')?>";
    }

    $.ajax({
      url : url,
      type: "POST",
      data : $('#form').serialize(),
      dataType: "JSON",
      success : function(data)
      {
        $('#modal-form').modal('hide');
        location.reload();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function delete_user(id_user)
  {
    if (confirm ('Anda yakin akan menghapus data ?'))
    {
      $.ajax({
        url : "<?php echo site_url('admin/C_Users/delete_user')?>/" + id_user,
        type : "POST",
        dataType : "JSON",
        success : function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error deleting data');
        }
      });
    }
  }
</script>
<!-- awal bootstrap modal -->
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="page-title">Tambah Data User</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="id_user"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Nama</label>
              <div class="col-md-9">
                <input name="nama" placeholder="Nama User" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Alamat</label>
              <div class="col-md-9">
                <textarea name="alamat" type="text" class="form-control" placeholder="Alamat User"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Email</label>
              <div class="col-md-9">
                <input name="email" placeholder="Email User" class="form-control" type="email">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">No Telepon</label>
              <div class="col-md-9">
                <input name="no_telp" placeholder="Nomor Telepon" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Password</label>
              <div class="col-md-9">
                <input name="password" placeholder="Password untuk user" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Level user</label>
              <div class="col-md-9">
                <select class="form-control" name="level">
                  <option value="">--pilih level untuk user</option>
                  <option value="admin">Admin</option>
                  <option value="operator">Operator</option>
                </select>
              </div>
            </div>
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrows-alt"></i> Batal</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
  </body>
</html>
