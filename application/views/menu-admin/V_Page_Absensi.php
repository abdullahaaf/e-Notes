<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
  <title>Absen Peserta | e-Notes</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/linearicons/style.css')?>">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css')?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/demo.css')?>">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/apple-icon.png')?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/img/note-icon.png')?>">
</head>

<body onload="startTime()">
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box lockscreen clearfix">
					<div class="content">
            <div class="logo text-center">
              <img src="<?php echo base_url('assets/img/logo-untag.png')?>" style="widht:80px; height : 80px;" alt="">
              <p class="subtile">Jurusan Teknik Informatika</p>
            </div>
						<div class="logo text-center">
              <p>Absensi Peserta Rapat pada:</p>
              <h3 class="title"><?php echo (date('d-m-Y'));?></h3>
              <p class="subtitle" id="txt"></p>
            </div>
						<form action="index.html">
							<div class="input-group">
								<input type="email" name"email" id="email" class="form-control" placeholder="Email peserta rapat ...">
								<span class="input-group-btn"><button type="submit" class="btn btn-primary"><i class="fa fa-arrow-right"></i></button></span>
							</div><br>
              <p class="text-center text-danger" style="display : none;"></p>
						</form>
            <div class="logo text-center">
              <p class="text-success" id="notification"></p>
            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->

  <!-- javascript area -->
  <script type="text/javascript">

  $( document ).ready(function() {
   $("#email").keyup(function(event){
      	if(event.keyCode == 13){
          	 cekMasuk();
      	}
  	});
  });

  function cekMasuk(){
  			var email=$("#email").val();
  		$.ajax({
  			url:'<?php echo site_url('admin/C_Page_Absensi/cekMasuk')?>',
  			type:'POST',
  			data:"email="+email,
  			success:function(data){
  			  	if(data==''){
  					 $( "#infodlg" ).html('Nik Tidak tersedia Harap Periksa Kembali ...');
  					 $( "#infodlg" ).dialog({ title:"Info...", draggable: false});
  				} else {
  				   $("#notification").html(data);
  				   $("#email").val("");
  				}
  			 }
  		});
  }

  function startTime()
  {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    // add a zero in front of numbers<10
    m=checkTime(m);
    s=checkTime(s);
    document.getElementById('txt').innerHTML=h+":"+m+":"+s;
    t=setTimeout(function(){startTime()},500);
  }

  function checkTime(i)
  {
    if (i<10)
      {
      i="0" + i;
      }
    return i;
  }

  </script>

  <script src="<?php echo base_url();?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/chartist/js/chartist.min.js"></script>
  <script src="<?php echo base_url();?>assets/scripts/klorofil-common.js"></script>
  <script src="<?php echo base_url();?>assets/datatables/jquery.dataTables.min.js"></script>

</body>

</html>
