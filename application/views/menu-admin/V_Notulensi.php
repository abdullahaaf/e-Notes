<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/linearicons/style.css')?>">
    <!-- date and time picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css')?>">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/demo.css')?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/datatables/css/jquery.dataTables.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>">

    <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/jquery.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/jquery.dataTables.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/apple-icon.png')?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assets/img/note-icon.png">
  </head>
  <body>

    <div id="wrapper">
      <!-- main -->
      <div class="main">
        <div class="main-content">
          <div class="container-fluid">
            <!-- awal panel -->
            <div class="panel panel-headline">
              <!-- header panel -->
              <div class="panel-heading">
                <h3 class="title">Notulensi Rapat</h3>
                <p class="subtitle">Admin dan operator dapat melakukan pencatatan hasil rapat</p>
              </div>
              <!-- end panel header -->
              <!-- panel body -->
              <div class="panel-body">
              <div class="alert alert-success" style="display: none;"></div>
              <div class="alert alert-warning" style="display: none;"></div>
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahNotulensi">Notulensi Baru</button>
              <hr/>
              <table class="table" id="tabel-notulensi">
                <thead>
                  <tr>
                    <td></td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="row">
                <?php foreach ($notulen as $rows): ?>
                  <div class="col-md-12">
                    <div class="panel panel-headline">
                      <div class="panel-heading bg-info">
                        <h3 class="panel-title"><?php echo $rows['judul'] ?></h3>
                        <p class="panel-subtitle">Penulis : <?php echo $rows['user_id'] ?></p>
                        <div class="right">
                          <a href="<?php echo base_url('index.php/admin/C_Notulensi/editNotulen/'.$rows['id_notulensi']) ?>"><span class="lnr lnr-pencil"></span></a>
                          <a href="<?php echo base_url('index.php/admin/C_Notulensi/hapusNotulensi/'.$rows['id_notulensi']) ?>" onclick="return confirm('apakah anda yakin akan menghapus data ini ?')"><span class="lnr lnr-trash"></span></a>
                        </div>
                      </div>
                      <div class="panel-body">
                        <h4>Tanggal Pelaksanaan <?php echo $rows['tanggal_notulensi'] ?></h4>
                        <p><?php echo $rows['deskripsi'] ?></p>
                      </div>
                    </div>
                  </div>
                <?php endforeach ?>
              </div>
                    </td>
                  </tr>
                </tbody>
              </table>
              </div>
              <!-- end panel body -->
            </div>
            <!-- end panel -->
          </div>
        </div>
      </div>
      <!-- end main -->
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="tambahNotulensi">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
          <button class="close" data-dismiss="modal">&times;</button>
            <h3 class="modal-title">Tambah Notulensi</h3>
          </div>
          <div class="modal-body">
            <form class="form-vertical" action="<?php echo base_url('index.php/admin/C_Notulensi/tambahNotulensi') ?>" method="POST">
              <div class="form-group">
                <label class="control-label">Nama Rapat</label>
                <input type="text" name="txtnamaRapat" class="form-control" placeholder="Nama rapat">
              </div>
              <div class="form-group">
                <label class="control-label">Tanggal Pelaksanaan</label>
                <input type="text" name="txtTanggal" class="form-control" placeholder="dd/mm/yyyy" id="txtTanggal">
              </div>
              <div class="form-group">
                <label class="control-label">Deskripsi Rapat</label>
                <textarea class="form-control" name="txtDeskripsi" rows="8" cols="6"></textarea>
              </div>
              <div class="form-group">
                <label class="control-label">Akses</label>
                <select class="form-control" name="hakAkses">
                  <option value="">Kategori AKses</option>
                  <option value="admin">Penuh</option>
                  <option value="member">User</option>
                </select>
              </div>
              <div class="form-group">
                <button class="btn btn-danger" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- javascript -->
    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/chartist/js/chartist.min.js"></script>
    <script src="<?php echo base_url();?>assets/scripts/klorofil-common.js"></script>
    <!-- date and time picker -->
    <script src="<?php echo base_url();?>assets/js/moment.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script>
    <script type="text/javascript">
      // untuk tanggal
      $('#txtTanggal').bootstrapMaterialDatePicker({ weekstart : 0, time : false});

      <?php if ($this->session->flashdata('sukses')): ?>
        $('.alert-success').html('<?php echo $this->session->flashdata('sukses') ?>').fadeIn().delay(2000).fadeOut('slow');
      <?php endif ?>
      <?php if ($this->session->flashdata('gagal')): ?>
        $('.alert-warning').html('<?php echo $this->session->flashdata('gagal') ?>').fadeIn().delay(2000).fadeOut('slow');
      <?php endif ?>
      <?php if ($this->session->flashdata('hapus')): ?>
        $('.alert-warning').html('<?php echo $this->session->flashdata('hapus') ?>').fadeIn().delay(2000).fadeOut('slow');
      <?php endif ?>
    </script>
    <script type="text/javascript">
      $(function () {
        $('#tabel-notulensi').dataTable();
      });
    </script>
  </body>
</html>
