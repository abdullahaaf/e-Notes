<!DOCTYPE html>
<html>
  <head>
    <title>Data Peserta Rapat | e-Notes</title>
    <meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  	<!-- VENDOR CSS -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
  	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css')?>">
  	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/linearicons/style.css')?>">
  	<!-- MAIN CSS -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css')?>">
  	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/css/demo.css')?>">
  	<!-- GOOGLE FONTS -->
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
  	<!-- ICONS -->
  	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/apple-icon.png')?>">
  	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assets/img/note-icon.png">
    <!-- DATATABLE -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/datatables/jquery.dataTables.min.css">
  </head>
  <body>

    <div id="wrapper">
      <!-- main -->
      <div class="main">
        <!-- main content -->
        <div class="main-content">
          <div class="container-fluid">
            <div class="panel panel-headline">
              <!-- panel header -->
              <div class="panel-heading">
                <h3 class="panel-title">Data Peserta Rapat</h3>
                <p class="panel-subtitle">Admin dan operator dapat mengatur data peserta rapat</p>
              </div>
              <form class="navbar-form navbar-left">
                <div class="input-group">
                  <span class="input-group-btn"><button type="button" name="button" onclick="add_peserta()" class="btn btn-success">Masukkan Data Peserta Rapat</button></span>
                </div>
              </form><br>
              <!-- end panel header -->
              <div class="panel-body">
                <div>
                  <table class="table table-striped-row" id="table">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Email</th>
                        <th>No Telepon</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- <?php $nomor = 1; ?>
                      <?php foreach ($peserta as $pes){?>
                        <tr>
                          <td><?php echo $nomor++; ?></td>
                          <td><?php echo $pes->nama; ?></td>
                          <td><?php echo $pes->alamat; ?></td>
                          <td><?php echo $pes->email; ?></td>
                          <td><?php echo $pes->no_telp; ?></td>
                          <td>
                            <button class="btn btn-warning btn-sm" onclick="edit_peserta(<?php echo $pes->id_prt;?>)"><i class="fa fa-pencil"></i></button>
           								 <button class="btn btn-danger btn-sm" onclick="delete_peserta(<?php echo $pes->id_prt;?>)"><i class="fa fa-trash"></i></button>
                          </td>
                        </tr>
                      <?php }?> -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end main content -->
      </div>
      <!-- end main -->
      </div>
    </div>

    <!-- operasi yang terhubung ke database -->
    <script type="text/javascript">
      $(document).ready(function(){
        $('#table').DataTable();
      });

      var save_method;
      var table;

      function add_peserta()
      {
        save_method = 'add';
        $('#form')[0].reset();
        $('#modal-form').modal('show');
      }

      function edit_peserta(id_prt)
      {
        save_method = 'update';
        $('#form')[0].reset();

        // load data from ajax
        $.ajax({
          url : "<?php echo site_url('admin/C_Data_Peserta/edit_peserta/')?>/" + id_prt,
          type : "GET",
          dataType : "JSON",
          success : function(data)
          {
            $('[name="id_prt"]').val(data.id_prt);
            $('[name="nama"]').val(data.nama);
            $('[name="alamat"]').val(data.alamat);
            $('[name="email"]').val(data.email);
            $('[name="no_telp"]').val(data.no_telp);

            $('#modal-form').modal('show');
            $('.page-title').text('Edit Data Peserta');
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
      }

      function save()
      {
        var url;
        if (save_method == 'add')
        {
          url = "<?php echo site_url('admin/C_Data_Peserta/add_peserta')?>";
        }else
        {
          url = "<?php echo site_url('admin/C_Data_Peserta/update_peserta')?>";
        }

        $.ajax({
          url : url,
          type : "POST",
          data : $('#form').serialize(),
          dataType : "JSON",
          success : function(data)
          {
            $('#modal-form').modal('hide');
            location.reload();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
      }

      function delete_peserta(id_prt)
      {
        if (confirm('Anda yakin ingin menghapus data ?'))
        {
          $.ajax({
            url : "<?php echo site_url('admin/C_Data_Peserta/delete_peserta')?>/" + id_prt,
            type : "POST",
            dataType : "JSON",
            success : function(data)
            {
              location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error deleting data');
            }
          });
        }
      }
    </script>

    <!-- bootstrap modal -->
    <div class="modal fade" id="modal-form" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="page-title">Data Peserta Rapat</h3>
          </div>
          <div class="modal-body form">
            <form action="#" id="form" class="form-horizontal">
              <input type="hidden" value="" name="id_prt"/>
              <div class="form-body">
                <div class="form-group">
                  <label class="control-label col-md-3">Nama</label>
                  <div class="col-md-9">
                    <input name="nama" placeholder="Nama Peserta" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Alamat</label>
                  <div class="col-md-9">
                    <textarea name="alamat" type="text" class="form-control" placeholder="Alamat Peserta"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Email</label>
                  <div class="col-md-9">
                    <input name="email" placeholder="Email Peserta" class="form-control" type="email">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">No Telepon</label>
                  <div class="col-md-9">
                    <input name="no_telp" placeholder="Nomor Telepon" class="form-control" type="text">
                  </div>
                </div>
              </div>
            </form>
              </div>
              <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrows-alt"></i> Batal</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
    </div>

    <!-- javascript -->
    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/chartist/js/chartist.min.js"></script>
    <script src="<?php echo base_url();?>assets/scripts/klorofil-common.js"></script>
    <script src="<?php echo base_url();?>assets/datatables/jquery.dataTables.min.js"></script>
  </body>
</html>
