<!doctype html>
<html lang="en">
<head>
	<title>Dashboard | e-Notes</title>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
						<div class="col-md-12">
							<!-- TODO LIST -->
							<div class="panel panel-headline">
								<div class="panel-heading">
									<h3 class="panel-title">Jadwal Rapat</h3>
								</div>
								<div class="panel-body">
									<ul class="list-unstyled todo-list">
										<?php foreach ($jadwal as $j) { ?>
											<li>
												<p>
													<span class="title"><?php echo $j->nama_rapat; ?></span>
													<span class="short-description"><?php echo $j->keterangan; ?></span>
													<span class="date"><?php echo $j->tanggal_rapat; ?> - <?php echo $j->jam; ?></span>
												</p>
											</li>
										<?php }?>
									</ul>
								</div>
							</div>
							<!-- END TODO LIST -->
						</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">&copy; 2017 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a>. All Rights Reserved.</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
</html>
