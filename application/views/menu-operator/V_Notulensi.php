<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/linearicons/style.css')?>">
    <!-- date and time picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css')?>">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/demo.css')?>">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/apple-icon.png')?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assets/img/note-icon.png">
  </head>
  <body>

    <div id="wrapper">
      <!-- main -->
      <div class="main">
        <div class="main-content">
          <div class="container-fluid">
            <!-- awal panel -->
            <div class="panel panel-headline">
              <!-- header panel -->
              <div class="panel-heading">
                <h3 class="title">Notulensi Rapat</h3>
                <p class="subtitle">Admin dan operator dapat melakukan pencatatan hasil rapat</p>
              </div>
              <!-- end panel header -->
              <!-- panel body -->
              <div class="panel-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label for="namaRapat" class="control-label col-sm-2">Data Rapat</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control" name="" value="" id="namaRapat" placeholder="Nama Rapat">
                    </div>
                    <div class="col-md-4">
                      <input type="text" class="form-control" name="" value="" id="tanggalRapat" placeholder="Tanggal Rapat">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="deskripsiRapat" class="control-label col-sm-2">Deskripsi Rapat</label>
                    <div class="col-md-8">
                      <textarea name="name" id="deskripsiRapat" rows="2" class="form-control" placeholder="Masukkan deskripsi rapat" cols="80"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="catatanRapat" class="control-label col-sm-2">Catatan Rapat</label>
                    <div class="col-md-8">
                      <textarea name="catatanRapat" id="catatanRapat" class="form-control" rows="8" cols="80"></textarea>
                    </div>
                  </div>
                </form>
              </div>
              <!-- end panel body -->
            </div>
            <!-- end panel -->
          </div>
        </div>
      </div>
      <!-- end main -->
    </div>

    <!-- javascript -->
    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/chartist/js/chartist.min.js"></script>
    <script src="<?php echo base_url();?>assets/scripts/klorofil-common.js"></script>
    <!-- date and time picker -->
    <script src="<?php echo base_url();?>assets/js/moment.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script>
    <script type="text/javascript">
      // untuk tanggal
      $('#tanggalRapat').bootstrapMaterialDatePicker({ weekstart : 0, time : false});
    </script>
  </body>
</html>
