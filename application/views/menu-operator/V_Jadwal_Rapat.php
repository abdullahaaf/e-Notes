<!DOCTYPE html>
<html>
  <head>
    <title>Jadwal Rapat | e-Notes</title>
  </head>
  <body>
    <div id="wrapper">
      <!-- main -->
      <div class="main">
        <div class="main-content">
          <div class="container-fluid">
            <!-- panel -->
            <div class="panel panel-headline">
              <!-- header panel -->
              <div class="panel-heading">
                <h3 class="title">Data Jadwal Rapat</h3>
                <p class="subtitle">Admin dan operator dapat mengatur jadwal untuk rapat</p>
              </div>
              <form class="navbar-form navbar-left">
                <div class="input-group">
                  <span class="input-group-btn"><button type="button" name="button" onclick="add_jadwal()" class="btn btn-success"><i class="fa fa-calendar-plus-o"> Tambah Data Jadwal</i></button></span>
                </div>
              </form><br>
              <!-- end header panel -->
              <!-- isi panel -->
              <div class="panel-body">
                <!-- end tombol tambah data -->
                <!-- table -->
                <table class="table table-striped-row" id="table">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Rapat</th>
                      <th>Tanggal Rapat</th>
                      <th>Jam</th>
                      <th>Keterangan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $nomor = 1; ?>
                    <?php foreach ($jadwal as $j) {?>
                      <tr>
                        <td><?php echo $nomor++; ?></td>
                        <td><?php echo $j->nama_rapat; ?></td>
                        <td><?php echo $j->tanggal_rapat; ?></td>
                        <td><?php echo $j->jam; ?></td>
                        <td><?php echo $j->keterangan; ?></td>
                        <td>
                          <button class="btn btn-warning btn-sm" onclick="edit_jadwal(<?php echo $j->id_jadwal;?>)"><i class="fa fa-pencil"></i></button>
                         <button class="btn btn-danger btn-sm" onclick="delete_jadwal(<?php echo $j->id_jadwal;?>)"><i class="fa fa-trash"></i></button>
                        </td>
                      </tr>
                    <?php }?>
                  </tbody>
                </table>
                <!-- end table -->
              </div>
              <!-- end isi panel -->
            </div>
            <!-- end panel -->
          </div>
        </div>
      </div>
      <!-- end main -->
    </div>

    <script type="text/javascript">
      $(document).ready(function(){
        $('#table').DataTable();
      });

      var save_method;
      var table;

      function add_jadwal()
      {
        save_method = 'add';
        $('#form')[0].reset();
        $('#modal-form').modal('show');
      }

      function edit_jadwal(id_jadwal)
      {
        save_method = 'update';
        $('#form')[0].reset();

        $.ajax({
          url : "<?php echo site_url('operator/C_Jadwal_Rapat/edit_jadwal/')?>/" + id_jadwal,
          type : "GET",
          dataType : "JSON",
          success : function (data)
          {
            $('[name="id_jadwal"]').val(data.id_jadwal);
            $('[name="nama_rapat"]').val(data.nama_rapat);
            $('[name="tanggal_rapat"]').val(data.tanggal_rapat);
            $('[name="jam"]').val(data.jam);
            $('[name="keterangan"]').val(data.keterangan);

            $('#modal-form').modal('show');
            $('.page-title').text('Edit Jadwal Rapat');
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
      }

      function save()
      {
        var url;
        if (save_method=='add')
        {
          url = "<?php echo site_url('operator/C_Jadwal_Rapat/add_jadwal')?>";
        }else
        {
          url = "<?php echo site_url('operator/C_Jadwal_Rapat/update_jadwal')?>";
        }

        $.ajax({
          url : url,
          type : "POST",
          data : $('#form').serialize(),
          dataType : "JSON",
          success : function (data)
          {
            $('#modal-form').modal('hide');
            location.reload();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
      }


      function delete_jadwal(id_jadwal)
      {
        if (confirm('Anda yakin ingin menghapus data ?'))
        {
          $.ajax({
            url : "<?php echo site_url('operator/C_Jadwal_Rapat/delete_jadwal')?>/" + id_jadwal,
            type : "POST",
            dataType : "JSON",
            success : function (data)
            {
              location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error deleting data');
            }
          });
        }
      }
    </script>

    <!-- boostrap modal -->
    <div class="modal fade" id="modal-form" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="page-title">Data Jadwal Rapat</h3>
          </div>
          <div class="modal-body form">
            <form class="form-horizontal" action="#" id="form">
              <input type="hidden" name="id_jadwal" value="">
              <div class="form-body">
                <div class="form-group">
                  <label class="control-label col-md-3">Nama Rapat</label>
                  <div class="col-md-9">
                    <input type="text" name="nama_rapat" class="form-control" placeholder="Nama Rapat">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Tanggal Rapat</label>
                  <div class="col-md-9">
                    <input type="text" name="tanggal_rapat" id="tanggal-rapat" placeholder="yyyy-mm-dd" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Jam</label>
                  <div class="col-md-9">
                    <input type="text" name="jam" id="jam" class="form-control timepicker">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Keterangan</label>
                  <div class="col-md-9">
                    <textarea name="keterangan" class="form-control" placeholder="keterangan"></textarea>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrows-alt"></i> Batal</button>
          </div>
        </div>
      </div>
    </div>

  <!-- javascript -->
  <!-- <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> -->

  <!-- DATEPICKER -->
  <script type="text/javascript">
  $(function(){
    $('#tanggal-rapat').datepicker({
      dateFormat : "yy-mm-dd"
    });
  });

  $(function(){
    $('#jam').timepicki();
  });
  </script>
  </body>
</html>
