<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/linearicons/style.css')?>">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css')?>">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/demo.css')?>">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/apple-icon.png')?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assets/img/note-icon.png">
  </head>
  <body>

    <div id="wrapper">
      <!-- main -->
      <div class="main">
        <div class="main-content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6">
                <!-- panel mengirim pesan -->
                <div class="panel panel-headline">
                  <!-- header panel -->
                  <div class="panel-heading">
                    <h3 class="title">Broadcast Jadwal Rapat</h3>
                    <p class="subtitle">Admin dan operator dapat mengumumkan jadwal rapat ke peserta rapat</p>
                  </div>
                  <!-- end panel heading -->
                  <!-- panel body -->
                  <div class="panel-body">
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label for="kepada" class="control-label col-sm-2">Kepada</label>
                        <div class="col-md-4">
                          <select class="form-control" id="kepada1" name="">
                            <option value="">Semua Peserta</option>
                            <option value="">Peserta Tertentu</option>
                          </select>
                        </div>
                        <div class="col-md-4">
                          <input type="text" class="form-control" name="" value="" placeholder="masukkan email penerima" disabled>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="textPesan" class="control-label col-sm-2">Text Pesan</label>
                        <div class="col-md-8">
                          <textarea name="name" rows="8" class="form-control" cols="80" placeholder="Masukkan isi pesan disini..."></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-12">
                          <div class="col-md-4"></div>
                          <div class="col-md-8">
                            <button type="button" class="btn btn-primary" name="button"><span class="fa fa-paper-plane-o"></span> Kirim</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- end panel body                 -->
                </div>
                </div>
                <!-- end panel mengirim pesan -->
                <!-- panel data notifikasi terkirim -->
                <div class="col-md-6">
                  <!-- panel -->
                  <div class="panel panel-headline">
                    <!-- header panel -->
                    <div class="panel-heading">
                      <h3 class="title">Data Notifikasi Terkirim</h3>
                    </div>
                    <!-- end panel heading -->
                    <!-- panel body -->
                    <div class="panel-body">
                      <form class="navbar-form navbar-right">
                        <div class="input-group">
                          <input type="text" value="" class="form-control" placeholder="Cari data...">
                          <span class="input-group-btn"><button type="button" class="btn btn-primary">Cari</button></span>
                        </div>
                      </form>
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <td>No</td>
                            <td>Kepada</td>
                            <td>Text Pesan</td>
                          </tr>
                        </thead>
                      </table>
                    </div>
                    <!-- end panel body                 -->
                  </div>
                </div>
                <!-- end panel data notifikasi terkirim -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end main -->
    </div>

    <!-- javascript -->
    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/chartist/js/chartist.min.js"></script>
    <script src="<?php echo base_url();?>assets/scripts/klorofil-common.js"></script>

  </body>
</html>
