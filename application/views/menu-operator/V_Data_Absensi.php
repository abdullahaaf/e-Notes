<!DOCTYPE html>
<html>
  <head>
    <title>Absensi Peserta | e-Notes</title>
  </head>
  <body>
    <div id="wrapper">
      <!-- main -->
      <div class="main">
        <div class="main-content">
          <div class="container-fluid">
            <div class="panel panel-headline">
              <div class="panel-heading">
                <h3 class="panel-title text-muted">Data Absensi Peserta Rapat</h3>
              </div>
              <!-- <p class="text-center">Rapat pada tanggal <?php echo (date('Y-m-d'));?></p> -->
              <div class="panel-body">
                <div class="navbar-form">
                </div>
                <table class="table table-hover" id="table">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Tanggal</th>
                      <th>Jam Kehadiran</th>
                      <th>Status Kehadiran</th>
                      <!-- <th>Aksi</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=0; foreach($absensi as $row) { $no++ ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $row['nama']; ?></td>
                      <td><?php echo $row['email']; ?></td>
                      <td><?php echo $row['tanggal']; ?></td>
                      <td><?php echo $row['jammasuk']; ?></td>
                      <td><?php if ($row['kodeabsensi'] == 1) { ?>
						          <h4><span class="label label-success">Hadir</span></h4>
                      <?php } else { ?>
                      <h4><span style="text-fonts:16px" class="label label-danger">Pulang</span></h4>
                      <?php } ?>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table><br>
                <div class="form-group col-md-12">
                  <label for="masukkan-email" class="text-center">Masukkan Email Peserta pada kolom di bawah ini</label>
                  <input type="text" name="email" class="form-control" id="masukkan-email" placeholder="Masukkan Email Peserta">
                </div>
                <div>
                  <p class="text-danger text-center" id="infodlg" style="dislay : none;"></p>
                </div>
                <div id="notification">
                  <!-- <p class="text-success text-center">Sukses melakukan absensi</p> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready(function(){
        $('#table').DataTable();
      });

      // untuk inputan user
      $( document ).ready(function() {
       $("#masukkan-email").keyup(function(event){
          	if(event.keyCode == 13){
              	 cekMasuk();
          	}
      	});
      });

      function cekMasuk()
      {
      		var email=$("#masukkan-email").val();
      		$.ajax({
      			url:'<?php echo site_url('operator/C_Data_Absensi/cekMasuk')?>',
      			type:'POST',
      			data:"email="+email,
      			success:function(data){
      			  	if(data==''){
      					 $( "#infodlg" ).html('Nik Tidak tersedia Harap Periksa Kembali ...');
      					 $( "#infodlg" ).dialog({ title:"Info...", draggable: false});
      				} else {
      				   $("#notification").html(data);
      				   $("#email").val("");
                 location.reload();
      				}
      			 }
      		});
      }
    </script>
  </body>
</html>
