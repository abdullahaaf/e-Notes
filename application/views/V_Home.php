<!DOCTYPE html>
<html>
  <head>
    <title>.: e-Notes :.</title>
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  	<!-- VENDOR CSS -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
  	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css')?>">
  	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/linearicons/style.css')?>">
  	<!-- MAIN CSS -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css')?>">
  	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/css/demo.css')?>">
  	<!-- GOOGLE FONTS -->
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
  	<!-- ICONS -->
  	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/apple-icon.png')?>">
  	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assets/img/note-icon.png">
  </head>
  <body style="background-image : url('<?php echo base_url('assets/img/untag2.jpg')?>'); background-size : cover;">
    <div id="wrapper">
      <div class="container" style="margin-top : 50px;">
          <div class="panel panel-headline" style="border : 2px; solid black;">
            <div class="panel-body">
              <div class="row">
                <div class="navbar navbar-left">
                  <img src="<?php echo base_url();?>assets/img/logo-untag.png" style = "height : 100px; width 100px;">
                </div>
                <div class="navbar navbar-left">
                  <h1 style="margin-left : 10px;"> Welcome to<br><span class="text-success">e - N o t e s</span></h1>
                </div>
                <div class="navbar navbar-right">
                  <h4 style="margin-right : 30px; margin-top : 30px;">Sistem Pencatatan Hasil Rapat <br>Jurusan Teknik Informatika <br>Universitas 17 Agustus Surabaya</h4>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-6">
                  <div class="panel" id ="panel-scrolling-demo" style="box-shadow : 5px 5px 5px; border : 1px solid gray;">
                    <div class="panel-heading">
                      <h3 class="page-title">Jadwal Rapat</h3>
                    </div>
                    <div class="panel-body">
                      <ul class="todo-list">
                        <?php foreach ($jadwal as $j) {?>
                          <li>
                            <p>
                              <span class="title"><?php echo $j->nama_rapat; ?></span>
                              <span class="short-description"><?php echo $j->keterangan; ?></span>
                              <span class="date"><?php echo $j->tanggal_rapat; ?> - <?php echo $j->jam; ?></span>
                            </p>
                          </li>
                        <?php }?>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel" id="panel-scrolling-demo" style="box-shadow : 5px 5px 5px; border : 1px solid gray;">
                    <div class="panel-heading">
                      <h3 class="page-title">Hasil Rapat</h3>
                    </div>
                    <div class="panel-body">
                      <p>Objectively network visionary methodologies via best-of-breed users. Phosfluorescently initiate go forward leadership skills before an expanded array of infomediaries. Monotonectally incubate web-enabled communities rather than process-centric.</p>
                      <p>Objectively network visionary methodologies via best-of-breed users. Phosfluorescently initiate go forward leadership skills before an expanded array of infomediaries. Monotonectally incubate web-enabled communities rather than process-centric.</p>
                      <p>Objectively network visionary methodologies via best-of-breed users. Phosfluorescently initiate go forward leadership skills before an expanded array of infomediaries. Monotonectally incubate web-enabled communities rather than process-centric.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="navbar navbar-right">
                  <a href="<?php echo site_url('C_Login');?>" class="btn btn-primary" style="margin-right : 30px;">Menuju halaman login</a>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    <!-- javascript -->
    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/chartist/js/chartist.min.js"></script>
    <script src="<?php echo base_url();?>assets/scripts/klorofil-common.js"></script>
  </body>
</html>
