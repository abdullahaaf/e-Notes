<!DOCTYPE html>
<html>
  <body>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth">
              <i class="lnr lnr-arrow-left-circle"></i></button>
          </div>
          <div class="navbar-left">
            <h1>.: e - N o t e s :.</h1>
          </div>
          <div id="navbar-menu">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user-circle"></i>
                  <span>Operator</span>
                  <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('C_Logout/logout');?>">
                    <i class="lnr lnr-exit"></i>
                    <span>Logout</span></a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </body>
</html>
