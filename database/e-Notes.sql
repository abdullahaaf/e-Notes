-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 27, 2017 at 01:15 
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-Notes`
--

-- --------------------------------------------------------

--
-- Table structure for table `foto_notulensi`
--

CREATE TABLE `foto_notulensi` (
  `id_foto` int(11) NOT NULL,
  `nama_foto` varchar(100) DEFAULT NULL,
  `lokasi` text,
  `keterangan_foto` text,
  `notulensi_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_rapat`
--

CREATE TABLE `jadwal_rapat` (
  `id_jadwal` int(11) NOT NULL,
  `nama_rapat` varchar(100) NOT NULL,
  `tanggal_rapat` date NOT NULL,
  `jam` varchar(20) NOT NULL,
  `keterangan` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_rapat`
--

INSERT INTO `jadwal_rapat` (`id_jadwal`, `nama_rapat`, `tanggal_rapat`, `jam`, `keterangan`) VALUES
(5, 'Rapat Kemahasiswaan', '2017-05-31', '01:01 PM', 'Coba');

-- --------------------------------------------------------

--
-- Table structure for table `notulensi`
--

CREATE TABLE `notulensi` (
  `id_notulensi` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `tanggal_notulensi` date DEFAULT NULL,
  `deskripsi` text,
  `permission` varchar(100) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `peserta`
--

CREATE TABLE `peserta` (
  `id_prt` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(300) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `no_telp` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peserta`
--

INSERT INTO `peserta` (`id_prt`, `nama`, `alamat`, `email`, `no_telp`) VALUES
(1, 'Dr Sudjiman', 'Surabaya', 'sudjiman@gmail.com', '081230675467'),
(2, 'Muhammad Abduh', 'Keputih Surabaya', 'abduh@gmail.com', '086731744982'),
(3, 'Sri Harini', 'Kertajaya - Surabaya', 'harini@gmail.com', '08573214567'),
(4, 'Muhammad Kurniawan S.Pd', 'Ketintang Surabaya', 'kurniawan@untag.ac.id', '085732144567');

-- --------------------------------------------------------

--
-- Table structure for table `tb_absensi`
--

CREATE TABLE `tb_absensi` (
  `id_abs` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `kodeabsensi` enum('1','2') NOT NULL,
  `jammasuk` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_absensi`
--

INSERT INTO `tb_absensi` (`id_abs`, `email`, `tanggal`, `kodeabsensi`, `jammasuk`) VALUES
(8, 'harini@gmail.com', '2017-05-24', '1', '23:29:48'),
(9, 'abduh@gmail.com', '2017-05-24', '1', '23:30:10'),
(10, 'kurniawan@untag.ac.id', '2017-05-24', '1', '23:30:29'),
(11, 'kurniawan@untag.ac.id', '2017-05-25', '1', '06:43:35'),
(12, 'abduh@gmail.com', '2017-05-25', '1', '06:43:55'),
(13, 'harini@gmail.com', '2017-05-25', '1', '06:44:09'),
(14, 'harini@gmail.com', '2017-05-27', '1', '13:02:54'),
(15, 'abduh@gmail.com', '2017-05-27', '1', '13:03:07'),
(16, 'kurniawan@untag.ac.id', '2017-05-27', '1', '13:03:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(10) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` text,
  `email` varchar(100) DEFAULT NULL,
  `no_telp` varchar(12) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `level` enum('admin','operator') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama`, `alamat`, `email`, `no_telp`, `password`, `level`) VALUES
(2, 'Abdullah', 'Sidayu - Gresik', 'abdullahaaf1@gmail.com', '081334270042', 'admin', 'admin'),
(4, 'Makhfud', 'Ngawi', 'makhfud@gmail.com', '08123067897', 'operator', 'operator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `foto_notulensi`
--
ALTER TABLE `foto_notulensi`
  ADD PRIMARY KEY (`id_foto`),
  ADD KEY `not_foto` (`notulensi_id`);

--
-- Indexes for table `jadwal_rapat`
--
ALTER TABLE `jadwal_rapat`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `notulensi`
--
ALTER TABLE `notulensi`
  ADD PRIMARY KEY (`id_notulensi`);

--
-- Indexes for table `tb_absensi`
--
ALTER TABLE `tb_absensi`
  ADD PRIMARY KEY (`id_abs`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `foto_notulensi`
--
ALTER TABLE `foto_notulensi`
  MODIFY `id_foto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jadwal_rapat`
--
ALTER TABLE `jadwal_rapat`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `notulensi`
--
ALTER TABLE `notulensi`
  MODIFY `id_notulensi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_absensi`
--
ALTER TABLE `tb_absensi`
  MODIFY `id_abs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
